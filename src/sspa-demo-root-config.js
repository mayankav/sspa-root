import { registerApplication, start } from "single-spa";

/* registerApplication({
  name: "@single-spa/welcome",
  app: () =>
    System.import(
      "https://unpkg.com/single-spa-welcome/dist/single-spa-welcome.js"
    ),
  activeWhen: ["/"],
}); */

registerApplication({
  name: "@sspa-demo/app1",
  app: () => System.import("@sspa-demo/app1"),
  activeWhen: ["/"],
  customProps: {
    testProp: "test prop",
  },
});

registerApplication({
  name: "@sspa-demo/sspa-app2",
  app: () => System.import("@sspa-demo/sspa-app2"),
  activeWhen: ["/"],
});

start({
  urlRerouteOnly: true,
});
